# README
Thank you for buying our product.
This theme adheres to [Magenest](https://store.magenest.com/).

### User guide
- If you have trouble installing this extension, please visit: 
http://confluence.izysync.com/display/DOC/Rope+Theme

- For installation guides of this theme, please visit: 
http://confluence.izysync.com/display/DOC/1.+Rope+Theme+Installation+Guides

- For detailed user guide of this extension, please visit: http://confluence.izysync.com/display/DOC/2.+Rope+Theme+User+Guides

- Support portal: http://servicedesk.izysync.com/servicedesk/customer/portal/5

- All the updates of this module are included in CHANGELOG.md file.
