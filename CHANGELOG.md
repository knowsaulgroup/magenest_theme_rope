# Change Log
All notable changes to this extension will be documented in this file.

## [2.0.0] - October 16.2017
- Compatible with Magento 2.2.0
## [1.0.1] - October 16.2017
- Fix bugs:
+ Add new Home store
+ Changed the style of some detail
## [1.0.0] - February 15.2016
- First Release
- Add Mega Menu extension
- Product Labels extension
- Add Blog extension