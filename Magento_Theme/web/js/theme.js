/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'jquery',
    'mage/smart-keyboard-handler',
    'mage/mage',
    'mage/ie-class-fixer',
    'owlCarousel',
    'domReady!'
], function ($, keyboardHandler) {
    'use strict';

    if ($('body').hasClass('checkout-cart-index')) {
        if ($('#co-shipping-method-form .fieldset.rates').length > 0 && $('#co-shipping-method-form .fieldset.rates :checked').length === 0) {
            $('#block-shipping').on('collapsiblecreate', function () {
                $('#block-shipping').collapsible('forceActivate');
            });
        }
    }

    $('.cart-summary').mage('sticky', {
        container: '#maincontent'
    });

    $('.panel.header .header.links').clone().appendTo('#store\\.links');

    $('[data-action="toggle-menu"]').click(function() {

        if ($('html').hasClass('nav-open')) {
            $('html').removeClass('nav-open');
            setTimeout(function () {
                $('html').removeClass('nav-before-open');
            }, 300);
        } else {
            $('html').addClass('nav-before-open');
            setTimeout(function () {
                $('html').addClass('nav-open');
            }, 42);
        }
    }) ;
    

    $(".footer-columns .widget ").click(function(){
        $(this).children(".block-content").slideDown();
    });

    $( ".megamenu" ).hover(
        function() {
            $( this ).children(".megamenu-content").css( "display" ,"block"); 
        }, function() {
            $( this ).children(".megamenu-content").css( "display" ,"none"); ;
        }
    );

    //minus and plus input number
   
        
        $('.btn-number').click(function(e){
            e.preventDefault();
            
            var fieldName = $(this).attr('data-field');
            var type      = $(this).attr('data-type');
            var input = $("input[name='"+fieldName+"']");
            var currentVal = parseInt(input.val());
            if (!isNaN(currentVal)) {
                if(type == 'minus') {
                    
                    if(currentVal > input.attr('min')) {
                        input.val(currentVal - 1).change();
                    } 
                    if(parseInt(input.val()) == input.attr('min')) {
                        $(this).attr('disabled', true);
                    }

                } else if(type == 'plus') {

                    if(currentVal < input.attr('maxlength')) {
                        input.val(currentVal + 1).change();
                    }
                    if(parseInt(input.val()) == input.attr('maxlength')) {
                        $(this).attr('disabled', true);
                    }

                }
            } else {
                input.val(0);
            }
        });
        $('.input-number').focusin(function(){
           $(this).data('oldValue', $(this).val());
        });
        $('.input-number').change(function() {
            
            var minValue =  parseInt($(this).attr('min'));
            var maxValue =  parseInt($(this).attr('maxlength'));
            var valueCurrent = parseInt($(this).val());
            
            name = $(this).attr('name');
            if(valueCurrent >= minValue) {
                $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
            } else {
                alert('Sorry, the minimum value was reached');
                $(this).val($(this).data('oldValue'));
            }
            if(valueCurrent <= maxValue) {
                $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
            } else {
                alert('Sorry, the maximum value was reached');
                $(this).val($(this).data('oldValue'));
            }
            
            
        });
        $(".input-number").keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                     // Allow: Ctrl+A
                    (e.keyCode == 65 && e.ctrlKey === true) || 
                     // Allow: home, end, left, right
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                         // let it happen, don't do anything
                         return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
        });

        $(".block-slide").owlCarousel({
            autoPlay : 4000,
            nav: true,
            navText : ["",""],
            singleItem : true,
            items: 1
        });

        $(".owl-client-say").owlCarousel({
            navText : ["",""],
            autoPlay : 4000,
            nav:false,
            singleItem : true,
            items: 1
        });
        $(".owl-dt").owlCarousel({
            items: 3,
            nav:false,
        });
         
        $('.tab-new-category .product-items').addClass('owl-carousel');
        $(".tab-new-category .product-items").owlCarousel({
            items: 4,
            navText : ["",""],
            nav:true,
            responsive:{
                0: {
                    items: 1,
                },
                360:{
                    items: 2,
                },
                600:{
                    items: 3,
                },
                992:{
                    items: 4,
                }
            }
        });
        $(".block-blog-style2 .post-list").owlCarousel({
            navText : ["",""],
            autoPlay : 4000,
            nav:false,
            singleItem : true,
            pagination: true,
        });

        $(".block-products.style1 .product-items").owlCarousel({
            pagination: true,
            items: 4,
            responsive:{
                0: {
                    items: 2,
                },
                600:{
                    items: 3,
                },
                992:{
                    items: 4,
                }
            }
        });

        $(".products-related .product-items, .products-upsell .product-items, .crosssell .product-items").owlCarousel({
            items: 4,
            nav : false,
            responsive:{
                0: {
                    items: 2,
                },
                600:{
                    items: 3,
                },
                992:{
                    items: 4,
                }
            }
        });

    keyboardHandler.apply();
});
