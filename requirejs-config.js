var config = {
    paths: {
        'owlCarousel': "js/owl.carousel"
    },
    shim: {
        'owlCarousel': {
            deps: ['jquery']
        }
    }
};